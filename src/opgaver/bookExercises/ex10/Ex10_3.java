package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_3 extends Exercise {

    public Ex10_3() {
        output = "------------------------ Exercise 10.3 ------------------------\n";
        ArrayList<String> list = new ArrayList<>();
        list.add("to");
        list.add("be");
        list.add("or");
        list.add("not");
        list.add("to");
        list.add("be");
        list.add("hamlet");
        output += Ex10_2.fancyPrintArrayList(list);
        list = removeOddLength(list);
        output += Ex10_2.fancyPrintArrayList(list);
    }

    public ArrayList<String> removeOddLength(ArrayList<String> list){
        ArrayList<String> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if(i%2!=0) newList.add(list.get(i));
        }
        return newList;
    }

}
