package opgaver.others.chainlistOgTests;

/**
 * Created by joneikholm on 15/09/15.
 */
public class Calculator
{


    /**
     * @pre ingen
     * @post return a+b
     * @param a
     * @param b
     * @return
     */
    public int add(int a, int b){
        return a+b;
    }

    /**
     * @pre b != 0
     * @post returns a/b
     * @param a
     * @param b
     * @return
     */
    public int divide(int a, int b){
        return a/b;
    }

    public int subtract(int a, int b){
        return a-b;
    }
}
