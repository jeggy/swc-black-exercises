package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_15 extends Exercise {

    public Ex10_15() {
        ArrayList list = new ArrayList();
        list.add(4);
        list.add(7);
        list.add(9);
        list.add(2);
        list.add(7);
        list.add(7);
        list.add(5);
        list.add(3);
        list.add(5);
        list.add(1);
        list.add(7);
        list.add(8);
        list.add(6);
        list.add(7);
        output = Ex10_5.fancyPrint(list);
        list = filterRange(list, 5, 7);
        output += Ex10_5.fancyPrint(list);

    }

    public ArrayList<Integer> filterRange(ArrayList<Integer> list, int min, int max){
        for (int i = 0; i < list.size(); i++) {
            Integer curr = list.get(i);
            if(curr>min&&curr<=max)
                list.remove(i--);
        }
        return list;
    }
}
