package view;

import config.Config;
import javafx.geometry.HPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import tools.FileObject;
import tools.Stuff;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jeggy on 9/11/15.
 */
public class BlackExercisesPanel extends BorderPane{

    private TextArea codeArea, outputArea;
    private Button previous, next;
    private Label current;
    private ImageView imgView;

    private int currentPlace = 0;
    private String currentDirectory = "ex10";
    private HashMap<String, ArrayList<FileObject>> files = Stuff.loadMapOpgaver(Config.BOOK_SRC);

    private MainView mv;

    public BlackExercisesPanel(MainView mv) {
        this.mv = mv;
        topBar();
        this.setTop(topBar());
        this.setCenter(center());
        this.setBottom(bottom());

        loadFile(currentPlace);

    }

    private ImageView bottom() {
        imgView = new ImageView();
        imgView.fitWidthProperty().bind(this.widthProperty());
        return imgView;
    }

    private VBox center() {
        VBox vb = new VBox();
        GridPane gp = new GridPane();

        this.codeArea = new TextArea("Code");
        this.outputArea = new TextArea("Output");
        this.codeArea.setPrefSize(Integer.MAX_VALUE, Integer.MAX_VALUE);
        this.outputArea.setPrefSize(Integer.MAX_VALUE, Integer.MAX_VALUE);
        gp.add(this.codeArea, 0, 0);
        gp.add(this.outputArea, 1, 0);


        vb.getChildren().addAll(gp, bottom());


        return vb;
    }

    private BorderPane topBar(){
        GridPane gp = new GridPane();

        ColumnConstraints columnSettings = new ColumnConstraints();
        columnSettings.setPercentWidth(33.3);
        gp.getColumnConstraints().add(columnSettings);
        gp.getColumnConstraints().add(columnSettings);
        gp.getColumnConstraints().add(columnSettings);

        previous = new Button("Previous");
        current = new Label("Current");
        next = new Button("Next");

        previous.setOnAction(event -> loadFile(--currentPlace));
        next.setOnAction(event -> loadFile(++currentPlace));
        previous.setStyle("-fx-font-size: 28px");
        current.setStyle("-fx-font-size: 28px");
        next.setStyle("-fx-font-size: 28px");


        GridPane.setHalignment(current, HPos.CENTER);
        GridPane.setHalignment(next, HPos.RIGHT);

        gp.add(previous, 0, 0);
        gp.add(current, 1, 0);
        gp.add(next, 2, 0);

        // MenuBar
        BorderPane bp = new BorderPane();
        bp.setCenter(gp);
        return bp;
    }

    public void loadFile(int fileIndex){
        mv.setPane("black");
        ArrayList<FileObject> list = files.get(currentDirectory);

        if(fileIndex<1)previous.setDisable(true);
        else previous.setDisable(false);

        if(fileIndex+2>list.size())next.setDisable(true);
        else next.setDisable(false);

        this.current.setText(list.get(fileIndex).getClassName());
        this.codeArea.setText(list.get(fileIndex).getClassContent());
        this.outputArea.setText(list.get(fileIndex).getClassOutput());
        try {
            this.imgView.setImage(new Image(list.get(fileIndex).getImgPath()));
        }catch(IllegalArgumentException e){
            this.imgView.setImage(null);
        }
    }

    public void setCurrentDirectory(String directory){
        this.currentDirectory = directory;
    }
}
