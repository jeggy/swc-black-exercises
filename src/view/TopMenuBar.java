package view;

import config.Config;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import tools.Stuff;

/**
 * Created by jeggy on 9/12/15.
 */
public class TopMenuBar extends MenuBar {

    private BlackExercisesPanel bev;

    public TopMenuBar() {
        super();

        // File Menu
        Menu fileMenu = new Menu("File");
        MenuItem exitMenuItem = new MenuItem("Exit");

        fileMenu.getItems().addAll(exitMenuItem);

        // Exercises Menu
        Menu exercisesMenu = new Menu("Exercises");
        Menu blackExercisesMenu = getBlackExercisesMenu();

        exercisesMenu.getItems().addAll(blackExercisesMenu);

        // Help Menu
        Menu helpMenu = new Menu("Help");
        MenuItem aboutItem = new MenuItem("About");

        helpMenu.getItems().addAll(aboutItem);

        // Add all Menu's to Menubar.
        this.getMenus().addAll(fileMenu, exercisesMenu, helpMenu);
    }



    private Menu getBlackExercisesMenu(){
        // Exercises
        Menu exerciseMenu = new Menu("Exercises");
        String[] exercises = Stuff.getDirectories(Config.BOOK_SRC);
        for (String exercise : exercises) {
            Menu exerciseItem = new Menu(exercise);
            exerciseMenu.getItems().addAll(exerciseItem);
            String[] files = Stuff.getFiles(Config.BOOK_SRC +exercise);
            for(int i = 0; i< files.length; i++){
                MenuItem item = new MenuItem(files[i].substring(0, files[i].length()-5));
                item.setMnemonicParsing(false);
                item.setId(i + "");
                item.setOnAction(event -> {
                    try {
                        String exFolder = exerciseItem.getText();

                        bev.setCurrentDirectory(exFolder);
                        bev.loadFile(Integer.parseInt(((MenuItem) event.getSource()).getId()));
                    } catch (NullPointerException e) {
                        System.out.println("Black Exercises not loaded yet. Use 'setBlackExercisesView' method to load them.");
                    }
                });
                exerciseItem.getItems().addAll(item);
            }
        }
        return exerciseMenu;
    }


    public void setBlackExercisesView(BlackExercisesPanel bev){
        this.bev = bev;
    }

}
