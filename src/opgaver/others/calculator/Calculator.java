package opgaver.others.calculator;

import tools.Exercise;

/**
 * Created by jeggy on 8/26/15.
 */
public class Calculator extends Exercise{

    private static String[] operators = {"+","-","*","/","^","√","(",")"};

    public static String calculate(String text, String pressed) {
        return (pressed.equals("=")) ? ""+doMath(text) : text+pressed;
    }

    private static double doMath(String text) {
        double leftNum, rightNum = 0.0;
        char operator = '+';

        try{leftNum = Double.parseDouble(text);}
        catch(NumberFormatException e){leftNum = 0.0;}


        for(int i = 0; i<text.length(); i++) {
            if(text.charAt(i)=='('){
                rightNum = doMath(StringTools.findEndParenthesis(text, i));
            }else if(StringTools.isInArray("" + text.charAt(i), operators)){
                leftNum = Double.parseDouble(text.substring(0,i));
                operator = text.charAt(i);
                rightNum = doMath(text.substring(i+1));
                break;
            }
        }
        System.out.println(leftNum+" | "+operator+" | "+rightNum);
        return calculate(leftNum, rightNum, operator);
    }


    private static double calculate(double num1, double num2, char operator) {
        double result = 0.0;
        switch (operator) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
            case '(':
            case ')':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
            case '^':
                result = Math.pow(num1, num2);
                break;
            case '√':
                result = num1 * Math.sqrt(num2);
                break;
        }

        return result;
    }

}
