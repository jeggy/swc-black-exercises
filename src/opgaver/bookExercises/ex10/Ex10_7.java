package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_7 extends Exercise {

    public Ex10_7() {
        ArrayList<String> list = new ArrayList<>();
        list.add("Be");
        list.add("Be");
        list.add("is");
        list.add("not");
        list.add("or");
        list.add("question");
        list.add("that");
        list.add("the");
        list.add("to");
        list.add("to");
        list.add("to");
        output = Ex10_2.fancyPrintArrayList(list);
        list = removeDuplicates(list);
        output += Ex10_2.fancyPrintArrayList(list);
    }


    public ArrayList<String> removeDuplicates(ArrayList<String> list){
        String prev = "";

        for (int i = 0; i<list.size(); i++) {
            String curr = list.get(i);
            if(prev.equals(curr)) list.remove(i--);
            prev = curr;
        }

        return list;
    }

}
