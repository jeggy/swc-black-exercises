package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_8 extends Exercise {

    public Ex10_8() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(9);
        list.add(6);
        list.add(2);
        list.add(4);
        list.add(8);
        list.add(3);
        list.add(1);
        output = Ex10_5.fancyPrint(list);
        list = removeOdd(list);
        output += Ex10_5.fancyPrint(list);
    }

    public ArrayList<Integer> removeOdd(ArrayList<Integer> list){
        for (int i = 0; i<list.size(); i++)
            if(list.get(i)%2==1) list.remove(i--);
        return list;
    }

}
