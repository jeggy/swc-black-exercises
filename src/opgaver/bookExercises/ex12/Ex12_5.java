package opgaver.bookExercises.ex12;

import tools.Exercise;

/**
 * Created by jeggy on 9/23/15.3:40 PM
 */
public class Ex12_5 extends Exercise{

    public Ex12_5() {

        writeBinary(0b1);
        output += "\n";
        writeBinary(44);
        output += "\n";
        writeBinary(255);
        output += "\n";

    }

    public void writeBinary(int number){
        if(number>0){
            writeBinary(number/2);
            output += number%2;
        }
    }

}
