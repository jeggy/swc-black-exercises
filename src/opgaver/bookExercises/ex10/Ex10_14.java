package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_14 extends Exercise {

    public Ex10_14() {
        ArrayList<String> list = new ArrayList<>();
        list.add("four");
        list.add("score");
        list.add("and");
        list.add("seven");
        list.add("years");
        list.add("ago");
        list.add("our");
        output = Ex10_2.fancyPrintArrayList(list);
        list = removeShorterStrings(list);
        output += Ex10_2.fancyPrintArrayList(list);

    }

    public ArrayList<String> removeShorterStrings(ArrayList<String> list){

        for(int i = 0; i<list.size()-1; i++){
            int removeIndex = (list.get(i).length()<list.get(i+1).length()) ? i : i+1;
            list.remove(removeIndex);
        }

        return list;
    }

}
