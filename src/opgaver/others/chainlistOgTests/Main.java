package opgaver.others.chainlistOgTests;

public class Main {

    public static void main(String[] args) {

        ChainList example = new ChainList(new DBManager());
        example.add(new Node("tadf1"));
        example.add(new Node("tadf2"));
        example.add(new Node("tadf3"));

        example.saveAllToDatabase();

    }
}
