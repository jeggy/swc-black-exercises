package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_11 extends Exercise {

    public Ex10_11() {
        ArrayList<String> list = new ArrayList<>();
        list.add("how");
        list.add("are");
        list.add("you?");
        output = Ex10_2.fancyPrintArrayList(list);
        list = stutter(list, 4);
        output += Ex10_2.fancyPrintArrayList(list);
    }

    public ArrayList<String> stutter(ArrayList<String> list, int k){
        for (int i = 0; i<list.size(); i++)
            for (int j = 0; j < k-1; j++)
                list.add(i, list.get(i++));
        return list;
    }
}
