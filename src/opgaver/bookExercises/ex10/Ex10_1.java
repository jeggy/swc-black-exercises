package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/10/15.
 */
public class Ex10_1 extends Exercise {

    private static char[] vovels = {'a', 'e', 'i', 'o', 'u'};

    public Ex10_1(){
        output = "------------------------ Exercise 10.1 ------------------------\n";
        ArrayList<String> list = new ArrayList<>();
        list.add("Word");
        list.add("Not a word");
        list.add("Some random text");
        list.add("Why not.");

        output += totalVowels(list); // Output
    }

    public int totalVowels(ArrayList<String> strings){
        if(strings.isEmpty()) return 0;

        int vovelsCount = 0;

        for (String s : strings) {
            for(char c : s.toCharArray()){
                if(charArrayContains(vovels, c)) vovelsCount++;
            }
        }
        return vovelsCount;
    }


    public boolean charArrayContains(char[] array, char ch){
        for (char c : array) {
            if(c==ch) return true;
        }
        return false;
    }

}

