package tools;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by jeggy on 9/11/15.
 */
public class Stuff {

    public static ArrayList<FileObject> loadOpgaver(String path){
        ArrayList<FileObject> files = new ArrayList();

        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        ArrayList<String> fileNames = new ArrayList();
        for (int i = 0; i<listOfFiles.length; i++) {
            fileNames.add(listOfFiles[i].getName());
        }

        Collections.sort(fileNames, new AlphanumComparator()); // The Alphanum Algorithm

        for (String name : fileNames) {

            files.add(new FileObject(path, name));
        }

        return files;
    }

    public static HashMap<String, ArrayList<FileObject>> loadMapOpgaver(String path){
        HashMap<String, ArrayList<FileObject>> files = new HashMap<>();

        for (String s : getDirectories(path)) {
            files.put(s, loadOpgaver(path+s+"/"));
        }

        return files;
    }

    public static String loadClassContent(String file) {
        try {
            return new String(Files.readAllBytes(Paths.get(file)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getOuputFromClass(String file) {
        try {
            String classPath = file.replace("/", ".");
            classPath = classPath.substring(4,classPath.length()-5);
            Object o = Class.forName(classPath).newInstance();
            return ((Exercise)o).getOutput();
        } catch (InstantiationException | IllegalAccessException | ClassCastException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] getDirectories(String path) {
        File[] directories = new File(path).listFiles(File::isDirectory);
        String[] names = new String[directories.length];
        for (int i = 0; i < directories.length; i++) names[i] = directories[i].getName();
        return names;
    }

    public static String[] getFiles(String path) {
        File[] directory = new File(path).listFiles();
        ArrayList<String> fileNames = new ArrayList<>();
        for (File file : directory) fileNames.add(file.getName());
        Collections.sort(fileNames, new AlphanumComparator());
        return fileNames.toArray(new String[]{});
    }
}
