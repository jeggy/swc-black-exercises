package opgaver.bookExercises.ex10;

import tools.Exercise;

/**
 * Created by jeggy on 9/14/15.
 */
public class Ex10_18 extends Exercise {


    public Ex10_18(){
        Point p = new Point();
        p.setLocation(25, 25);
        output = p.toString();

        output += p.distanceFromOrigin();
    }


}


class Point implements Comparable {

    private int x;
    private int y;

    // constructs a new point at the origin, (0, 0)
    public Point() {
        this(0, 0); // calls Point(int, int) constructor
    }

    // constructs a new point with the given (x, y) location
    public Point(int x, int y) {
        setLocation(x, y);
    }

    // returns the distance between this Point and (0, 0)
    public double distanceFromOrigin() {
        return Math.sqrt(x * x + y * y);
    }

    // returns the x-coordinate of this point
    public int getX() {
        return x;
    }

    // returns the y-coordinate of this point
    public int getY() {
        return y;
    }

    // sets this point's (x, y) location to the given values
    public void setLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // returns a String representation of this point
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    // shifts this point's location by the given amount
    public void translate(int dx, int dy) {
       setLocation(x + dx, y + dy);
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}