package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_10 extends Exercise {

    public Ex10_10() {
        ArrayList<String> list = new ArrayList<>();
        list.add("to");
        list.add("be");
        list.add("or");
        list.add("not");
        list.add("to");
        list.add("be");
        list.add("that");
        list.add("is");
        list.add("the");
        list.add("question");
        output = Ex10_2.fancyPrintArrayList(list);
        list = removeInRange(list, "free", "rich");
        output += Ex10_2.fancyPrintArrayList(list);
    }

    public ArrayList<String> removeInRange(ArrayList<String> list, String firstWord, String secondWord){
        for (int i = 0; i<list.size(); i++) {
            String curr = list.get(i);
            if (curr.charAt(0) > firstWord.charAt(0) && curr.charAt(0) < secondWord.charAt(0))
                list.remove(i--);
        }
        return list;
    }

}
