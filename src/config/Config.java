package config;

/**
 * Created by jeggy on 9/20/15. 11:05 AM
 */
public class Config {

    public static final String BOOK_SRC = "src/opgaver/bookExercises/";
    public static final String OTHERS_SRC = "src/opgaver/others/";

}
