package opgaver.others.calculator;

/**
 * Created by jeggy on 8/28/15.
 */
public class StringTools {

    public static boolean isInArray(String text, String[] array){
        for(String a : array) if(text.equals(a)) return true;
        return false;
    }

    // TODO: Not done.
    public static String findEndParenthesis(String text, int startPoint) {
        int startCounter = 0;

        String result = "";

        for(int i = startPoint+1; i<text.length(); i++){
            if(text.charAt(i)=='(') startCounter++;
            else if(text.charAt(i)==')'){
                if(startCounter>0) startCounter--;
                else{
                    result = text.substring(startPoint+1,i);
                }
            }
        }
        System.out.println(result);

        return result;
    }

}
