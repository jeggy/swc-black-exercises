package opgaver.others.calculator;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        System.out.println("Jeggy Branch");

        BorderPane bp = new BorderPane();
        MainPanel calcScene = new MainPanel();
        bp.setTop(menuBar(primaryStage));
        bp.setCenter(calcScene);
        calcScene.getStylesheets().add("opgaver/others/calculator/style.css");

        Scene mainScene = new Scene(bp, 320, 250);

        primaryStage.setScene(mainScene);
        primaryStage.setTitle("Calculator");
        primaryStage.setResizable(false);


        primaryStage.show();
    }

    private MenuBar menuBar(Stage s) {
        MenuBar mb = new MenuBar();
        mb.prefWidthProperty().bind(s.widthProperty());

        MenuBarHandler mbh = new MenuBarHandler();

        // File menu
        Menu fileMenu = new Menu("File");
        MenuItem exitItem = new MenuItem("Exit");

        fileMenu.getItems().addAll(exitItem);

        // Help menu
        Menu helpMenu = new Menu("Help");
        MenuItem knownBugsItem = new MenuItem("Known Bugs");
        MenuItem helpItem = new MenuItem("Help");

        helpMenu.getItems().addAll(knownBugsItem, helpItem);


        // Add Event Handler to all
        exitItem.setOnAction(mbh);
        knownBugsItem.setOnAction(mbh);
        helpItem.setOnAction(mbh);

        // Add menu's to menuBar
        mb.getMenus().addAll(fileMenu, helpMenu);

        return mb;
    }
}
