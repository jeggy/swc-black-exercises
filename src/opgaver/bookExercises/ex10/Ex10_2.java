package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_2 extends Exercise {

    public Ex10_2() {
        output += "------------------------ Exercise 10.2 ------------------------\n";
        ArrayList<String> list = new ArrayList<>();
        list.add("to");
        list.add("be");
        list.add("or");
        list.add("not");
        list.add("to");
        list.add("be");
        list.add("hamlet");
        output += fancyPrintArrayList(list);
        swapPairs(list);
        output += fancyPrintArrayList(list);
    }

    public void swapPairs(ArrayList<String> list){
        for (int i = 0; i < list.size()-1; i+=2) {
            String tmp1 = list.get(i);
            list.set(i, list.get(i + 1));
            list.set(i + 1, tmp1);
        }
    }

    public static String fancyPrintArrayList(ArrayList<String> list){
        String r = "";
        r += "--------------------\n";
        for (String s : list) {
            r += s+"\n";
        }
        r += "--------------------\n";
        return r;
    }

}
