package opgaver.others.chainlistOgTests;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by joneikholm on 15/09/15.
 */
public class CalculatorTest
{
    private Calculator calculator;
    @Before
    public void setUp() throws Exception {
        calculator= new Calculator();
    }

    @Test
    public void testAdd() throws Exception {
        assertNotNull(new Object());
        assertTrue(5>2);
        assertEquals(4, calculator.add(2,2));
    }
    @Test (expected = Exception.class)
    public void testDivide() throws Exception {
        calculator.divide(5,0);  //
    }
}