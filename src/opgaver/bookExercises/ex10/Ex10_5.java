package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_5 extends Exercise {

    public Ex10_5() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(4);
        list.add(1);
        list.add(2);
        list.add(0);
        list.add(3);
        output = fancyPrint(list);
        list = scaleByK(list);
        output += fancyPrint(list);
    }

    public ArrayList<Integer> scaleByK(ArrayList<Integer> list){
        for (int i = 0; i<list.size(); i++) {
            int num = list.get(i);
            list.remove(i--);
            for (int j = 0; j<num; j++){
                list.add(++i, num);
            }
        }

        return list;

        /*ArrayList<Integer> integers = new ArrayList<>();

        for (Integer i : list) {
            for (int j = 0; j < i; j++) {
                integers.add(i);
            }
        }

        return integers;*/
    }


    public static String fancyPrint(ArrayList<Integer> list){
        String r;
        r = "--------------------\n";
        for (Integer i : list) {
            r += i+"\n";
        }
        r += "--------------------\n";

        return r;
    }
}
