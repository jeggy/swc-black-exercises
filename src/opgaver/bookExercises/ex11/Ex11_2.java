package opgaver.bookExercises.ex11;

import opgaver.bookExercises.ex10.Ex10_5;
import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/14/15.
 */
public class Ex11_2 extends Exercise{

    public Ex11_2() {
        ArrayList<Integer> list1 = new ArrayList();
        ArrayList<Integer> list2 = new ArrayList();
        list1.add(1);   list2.add(6);
        list1.add(2);   list2.add(7);
        list1.add(3);   list2.add(8);
        list1.add(4);   list2.add(9);
        list1.add(5);   list2.add(10);
                        list2.add(11);
                        list2.add(12);
        output += fancyPrint(list1, list2);
        ArrayList<Integer> list = alternate(list1, list2);
        output += Ex10_5.fancyPrint(list);
    }

    public <T> ArrayList<T> alternate(ArrayList<T> l1, ArrayList<T> l2){
        ArrayList<T> list = new ArrayList<>();

        for (int i = 0; i < ((l1.size() > l2.size()) ? l1.size() : l2.size()); i++) {
            if(l1.size()>i) list.add(l1.get(i));
            if(l2.size()>i) list.add(l2.get(i));
        }

        return list;
    }



    public static String fancyPrint(ArrayList<?> l1, ArrayList<?> l2){
        String output = "--------------------\n";
        for (int i = 0; i < ((l1.size() > l2.size()) ? l1.size() : l2.size()); i++) {
            if(l1.size()>i) output+=l1.get(i);
            output+="\t\t";
            if(l2.size()>i) output+=l2.get(i);
            output+="\n";
        }
        output += "--------------------\n";
        return output;
    }

}
