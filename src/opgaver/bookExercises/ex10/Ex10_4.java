package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_4 extends Exercise {

    public Ex10_4() {
        ArrayList<String> list = new ArrayList<>();
        list.add("Java");
        list.add("Program");
        output = Ex10_2.fancyPrintArrayList(list);
        quadList(list);
        output += Ex10_2.fancyPrintArrayList(list);
    }

    public void quadList(ArrayList<String> list){
        for (int i = 0; i<list.size(); i++)
            for (int j = 1; j < 4; j++)
                list.add(i, list.get(i++));
    }
}
