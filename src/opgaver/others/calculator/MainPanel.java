package opgaver.others.calculator;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

/**
 * Created by jeggy on 8/25/15.
 */
public class MainPanel extends BorderPane {

    private CTextField topBarField = new CTextField();

    public MainPanel(){
        this.setTop(topBarField);
        this.setCenter(this.buttonPanel());
    }


    public GridPane buttonPanel(){
        GridPane pane = new GridPane();

        // GridPane setup
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(4);
        pane.setVgap(4);

        // Buttons
        String[] btnsTxt = {"7","8","9","/","←","C",
                            "4","5","6","*","(",")",
                            "1","2","3","-","x²","√",
                            "0",".","%","+","="};

        Handler handler = new Handler(this.topBarField);
        for(int i = 0; i<btnsTxt.length; i++){
            Button btn = new Button(btnsTxt[i]);

            btn.setMinSize(50,40);
            btn.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            btn.getStyleClass().add("button");
            btn.setOnAction(handler);

            if(i==btnsTxt.length-1) pane.add(btn, i%6, i/6, 2, 1);
            else pane.add(btn, i%6, i/6);
        }

        return pane;
    }

}
