package opgaver.others.recursive;

/**
 * Created by jeggy on 9/22/15.11:49 AM
 */
public class Main {

    public static void main(String[] args) {
        String words = "A few words here and there.";
        System.out.println(words);
        System.out.println(backwards(words));

        System.out.println("------------------------------------------------------------");

        words = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dictum est ac magna fermentum tempor.";
        System.out.println(words);
        System.out.println(backwards(words));

    }


    public static String backwards(String text){
        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i)==' '){
                text = backwards(text.substring(++i)) + text.substring(0, i);
                break;
            }
        }
        return text;
    }

}
