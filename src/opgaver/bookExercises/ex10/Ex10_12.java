package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_12 extends Exercise {

    public Ex10_12() {
        ArrayList<String> list = new ArrayList<>();
        list.add("this");
        list.add("is");
        list.add("lots");
        list.add("of");
        list.add("fun");
        list.add("for");
        list.add("Java");
        list.add("coders");
        output = Ex10_2.fancyPrintArrayList(list);
        list = markLength4(list);
        output += Ex10_2.fancyPrintArrayList(list);
    }

    public ArrayList<String> markLength4(ArrayList<String> list){
        for (int i = 0; i < list.size(); i++)
            if(list.get(i).length()==4)
                list.add(i++, "****");
        return list;
    }

}
