package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_13 extends Exercise {

    public Ex10_13() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(8);
        list.add(19);
        list.add(42);
        list.add(7);
        list.add(26);
        list.add(19);
        list.add(-8);

        output = Ex10_5.fancyPrint(list);
        list = reverse3(list);
        output += Ex10_5.fancyPrint(list);
    }

    public ArrayList<Integer> reverse3(ArrayList<Integer> list){
        for (int i = 0; i < list.size()-2; i+=3) {
            Integer tmp = list.get(i+2);
            list.set(i+2, list.get(i));
            list.set(i, tmp);
        }
        return list;
    }

}
