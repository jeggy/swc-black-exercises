package opgaver.others.chainlistOgTests;

public class Node<T>
{
    Node next;
    T item;

    public Node(T i, Node n){
        item=i;
        next=n;
    }

    public Node(T i){
        item=i;
    }

}
