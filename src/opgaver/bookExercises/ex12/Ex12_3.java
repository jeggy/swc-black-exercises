package opgaver.bookExercises.ex12;

import tools.Exercise;

/**
 * Created by jeggy on 9/23/15.12:54 PM
 */
public class Ex12_3 extends Exercise{

    public Ex12_3() {
        writeSequence(1);
        output += "\n";
        writeSequence(2);
        output += "\n";
        writeSequence(3);
        output += "\n";
        writeSequence(4);
        output += "\n";
        writeSequence(5);
        output += "\n";
        writeSequence(6);
        output += "\n";
        writeSequence(7);
        output += "\n";
        writeSequence(8);
        output += "\n";
        writeSequence(9);
        output += "\n";
        writeSequence(10);
        output += "\n";
    }

    public void writeSequence(int n){
        if (n < 1) throw new IllegalArgumentException("Argument passed needs to be 1 or more");
        else if (n == 1) output += "1";
        else if (n == 2) output += "1 1";
        else {
            int tmp = (n+1)/2;
            output += tmp+" ";
            writeSequence(n-2);
            output += " "+tmp;
        }
    }
}
