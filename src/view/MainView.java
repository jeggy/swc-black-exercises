package view;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

import java.util.HashMap;

/**
 * Created by jeggy on 9/16/15.4:36 PM
 */
public class MainView extends BorderPane{

    private HashMap<String, Pane> panes = new HashMap();
    private TopMenuBar tmb;

    public MainView() {
        tmb = new TopMenuBar();

        this.makePanes();
        this.setTop(tmb);

        this.setPane("start");
    }

    // fix for BlackExercisesPanel on window resize.
    public void size(double width){
        this.panes.get("black").setMinWidth(width);
        this.panes.get("black").setPrefWidth(width);
        this.panes.get("black").setMaxWidth(width);
    }

    private void makePanes(){
        BlackExercisesPanel blacks = new BlackExercisesPanel(this);

        tmb.setBlackExercisesView(blacks); // Load stuff

        MainPanel mp = new MainPanel();

        panes.put("start", mp);
        panes.put("black", blacks);
    }

    public Pane getPane(String pane){
        return panes.get(pane);
    }

    public void setPane(String pane){
        if(panes.containsKey(pane)){
            this.setCenter(this.getPane(pane));
        }
    }

}
