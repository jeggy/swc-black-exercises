package tools;

import tools.Stuff;

/**
 * Created by jeggy on 9/11/15.
 */
public class FileObject {

    private String classPath, classContent, className, classOutput, imgPath;

    public FileObject(String classPath, String className) {
        this.className = className;
        this.classPath = classPath;
        this.classContent = Stuff.loadClassContent(this.classPath + this.className);
        this.classOutput = Stuff.getOuputFromClass(classPath + className);
        this.imgPath = "res/"+className.substring(0,className.length()-4)+"png";
    }

    public String getClassPath() {
        return classPath;
    }

    public String getClassContent() {
        return classContent;
    }

    public String getClassName() {
        return className;
    }

    public String getClassOutput() {
        return classOutput;
    }

    public String getImgPath() {
        return imgPath;
    }

    @Override
    public String toString() {
        return className;
    }
}
