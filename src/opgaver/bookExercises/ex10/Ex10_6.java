package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_6 extends Exercise {

    public Ex10_6() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(-25);
        list.add(-39);
        list.add(-50);
        list.add(-3);
        list.add(-8);
        list.add(-24);
        list.add(-4);
        output = Ex10_5.fancyPrint(list);
        list = maxToFront(list);
        output += Ex10_5.fancyPrint(list);
    }

    public ArrayList<Integer> maxToFront(ArrayList<Integer> list){

        Integer max = Integer.MIN_VALUE;
        int prevIndex = -1;
        for (int i = 0; i < list.size(); i++) {
            Integer curr = list.get(i);
            if(curr>max){
                prevIndex = i;
                max = curr;
            }
        }

        if(prevIndex>0){
            list.remove(prevIndex);
            list.add(0, max);
        }
        return list;
    }

}
