package opgaver.others.chainlistOgTests;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by joneikholm on 15/09/15.
 */
public class ChainListTest
{

    ChainList chainList;
    private DBManager dbManagerMock;
    private ArgumentCaptor<Node> nodeArguments;

    @Before
    public void setUp() throws Exception {
        // Create a mock object of DBManager.
        dbManagerMock = mock(DBManager.class);

        // Create chainList
        chainList = new ChainList(dbManagerMock);
        chainList.add(new Node("Test 1"));
        chainList.add(new Node("Test 2"));

        nodeArguments = ArgumentCaptor.forClass(Node.class);
    }

    @Test
    public void testSaveNode() throws Exception{
        chainList.saveAllToDatabase();
        verify(dbManagerMock, times(chainList.getNumItems())).saveNode(nodeArguments.capture());
    }

    @Test
    public void testGetNumItems() throws Exception {
        assertEquals(0, chainList.getNumItems());
        chainList.add(new Node(3232));
        assertEquals(1, chainList.getNumItems());

    }

    @Test
    public void testAdd() throws Exception {
        chainList.add(new Node(2341));
       // assertNotNull(chainList.find(0));
    }

    @Test
    public void testGet() throws Exception {
        chainList.add(new Node("Text"));
        assertNotNull(chainList.getItem(0));
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testGet1() throws Exception {
        assertNotNull(chainList.getItem(0));
    }

    @Test
    public void testAdd1() throws Exception {
        chainList.add(new Node("asdf"));
        assertTrue(chainList.toString().getClass().getSimpleName().equals("String"));
    }

    @Test
    public void testToString() throws Exception {

    }
}