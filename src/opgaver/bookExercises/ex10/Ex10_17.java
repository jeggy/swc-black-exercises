package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_17 extends Exercise {

    public Ex10_17() {
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();
        list1.add(10);
        list1.add(20);
        list1.add(30);
        list2.add(4);
        list2.add(5);
        list2.add(6);
        list2.add(7);
        list2.add(8);
        output = Ex10_5.fancyPrint(list1);
        output += Ex10_5.fancyPrint(list2);
        ArrayList<Integer> newList = interleave(list1, list2);
        output += Ex10_5.fancyPrint(newList);
    }

    public ArrayList<Integer> interleave(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        ArrayList<Integer> list = new ArrayList<>();
        int longerList = (list1.size() > list2.size()) ? list1.size() : list2.size();
        for (int i = 0; i < longerList; i++) {
            if (i < list1.size())
                list.add(list1.get(i));
            if (i < list2.size())
                list.add(list2.get(i));
        }
        return list;
    }
}
