package opgaver.bookExercises.ex10;

import tools.Exercise;

import java.util.ArrayList;

/**
 * Created by jeggy on 9/11/15.
 */
public class Ex10_9 extends Exercise {

    public Ex10_9() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(4);
        list.add(2);
        list.add(2);
        list.add(8);
        list.add(2);
        list.add(3);
        list.add(2);
        list.add(4);
        list.add(8);
        list.add(2);
        output = Ex10_5.fancyPrint(list);
        output += rangeBetweenEvens(list);

    }

    public int rangeBetweenEvens(ArrayList<Integer> list){

        int firstEven = -1;
        int lastEven = -1;
        for (int i = 0; i < list.size(); i++) {
            int curr = list.get(i);
            if(curr%2==0){
                if(firstEven==-1)
                    firstEven = i;
                lastEven = i;
            }
        }

        return lastEven-firstEven+1; // Stupid logic.
    }

}
