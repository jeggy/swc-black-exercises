import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import opgaver.bookExercises.ex12.*;
import view.MainView;

public class Main extends Application{

    public static void main(String[] args) {

//        System.out.println(new Ex12_5().getOutput());
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("SWC Exercises - Jógvan Olsen");
        MainView mv = new MainView();
        Scene s = new Scene(mv, 960, 680);
        s.widthProperty().addListener((observableValue, oldSceneWidth, newSceneWidth) -> {
            mv.size(newSceneWidth.doubleValue());
        });
        primaryStage.setScene(s);
        primaryStage.show();
    }
}
