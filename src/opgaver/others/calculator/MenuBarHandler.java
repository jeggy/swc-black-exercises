package opgaver.others.calculator;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

/**
 * Created by jeggy on 8/28/15.
 */
public class MenuBarHandler implements EventHandler {


    @Override
    public void handle(Event event) {
        String pressed = ((MenuItem)event.getSource()).getText();

        switch (pressed){
            case "Exit":
                Platform.exit();
                break;
            case "Known Bugs":
                System.out.println("'(',')','%' og '√' rigga ikki.");
                break;
            default:
                System.out.println("'"+pressed+"'"+" MenuItem hasn't been added to the event handler");
        }

    }
}
