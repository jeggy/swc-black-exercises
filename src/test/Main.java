package test;

import tools.Stuff;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by jeggy on 9/21/15.4:06 PM
 */
public class Main {

    public static void main(String[] args) throws IOException {

        String[] files = Stuff.getFiles("src/test/dk");

        for (String arg : files) {
            String filesContent = Stuff.loadClassContent("src/test/dk/"+arg);
            arg = arg.replace("da-DK", "fo-FO");


            FileWriter fr = new FileWriter("src/test/fo/"+arg); // after '.' write
//your file extention (".txt" in this case)
            fr.write(filesContent); // warning: this will REPLACE your old file content!
            fr.close();
        }

    }

}
