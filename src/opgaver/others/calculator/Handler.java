package opgaver.others.calculator;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * Created by jeggy on 8/26/15.
 */
public class Handler implements EventHandler<ActionEvent> {

    private TextField field;

    public Handler(TextField field){
        this.field = field;
    }

    @Override
    public void handle(ActionEvent event) {
        String pressed = ((Button)event.getSource()).getText();
        String currentText = field.getText();

        String result;

        switch (pressed){
            case "C":
                result = "";
                break;
            case "←":
                result = (currentText.length()>0) ? currentText.substring(0, currentText.length()-1) : "";
                break;
            case "x²":
                result = currentText+"^2";
                break;
            case "=":
                result = Calculator.calculate(field.getText(), pressed);
                break;
            default:
                result = currentText+pressed;
        }

        field.setText(result);
    }
}
