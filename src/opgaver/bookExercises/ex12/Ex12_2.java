package opgaver.bookExercises.ex12;

import tools.Exercise;

/**
 * Created by jeggy on 9/23/15.12:38 PM
 */
public class Ex12_2 extends Exercise{

    public Ex12_2() {
        writeNums(5);
    }

    public void writeNums(int n){
        if(n>=1) {
            output += n + ((--n >= 1) ? ", " : "");
            if (n > 0) writeNums(n);
        }else throw new IllegalArgumentException("Argument passed needs to be 1 or more");
    }
}

