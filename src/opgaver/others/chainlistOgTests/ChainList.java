package opgaver.others.chainlistOgTests;

/**
 * Created by joneikholm on 11/09/15.
 */
public class ChainList
{
    private Node head;
    private Node tail;
    private DBManager dbManager;

    public int getNumItems() {
        return numItems;
    }

    private int numItems;
    public ChainList(DBManager dbManager){
        head=null;
        tail=null;
        this.dbManager = dbManager;
    }

    public boolean saveAllToDatabase(){
        Node curr = head;
        for (int i = 0; i < numItems; i++) {
            boolean saved = dbManager.saveNode(curr);
            System.out.println((saved) ? "\tSuccessfully saved '"+curr.item+"'." : "\tFailed to save node '"+curr.item+"'");
            curr = curr.next;
            System.out.println();
        }
        return false;
    }

    public void add(Node item, int index){
        // tilføj Node på plads index
        if(index==0){
            if(head==null)
                tail=item;
            item.next=head;
            head=item;
            // hvis den er tom, så skal tail opdateres
        }else {
            Node curr = find(index - 1);
            item.next = curr.next;
            curr.next = item;
        }
        numItems++;
    }

    public Node getItem(int index){  //encapsulation
        if(head==null ||!(index>=0 && index<numItems)){
            throw new IndexOutOfBoundsException();
        }else{
            return find(index);
        }
    }

    private Node find(int index){
        Node curr=head;
        for (int i = 0; i <index ; i++) {

            curr= curr.next;
        }
        return curr;
    }

    public void add(Node item){
        if(head==null){
            head=item;
            tail=item;
        }else{// hvis head ikke er NULL
            tail.next=item;
            tail=item;
        }
        numItems++;
    }

    public String toString(){
        String output="";
        Node curr=head;
        for (int i = 0; i <numItems ; i++) {
            output+= ", "+curr.item;
            curr= curr.next;
        }
        return output.substring(1);
    }

    public void printAll() {
        Node curr = head;
        for (int i = 0; i < numItems; i++) {
            System.out.println(curr.item);
            curr = curr.next;
        }
    }
}
