package opgaver.bookExercises.ex12;

import tools.Exercise;

/**
 * Created by jeggy on 9/23/15.1:49 PM
 */
public class Ex12_4 extends Exercise{

    public Ex12_4() {
        output += doubleDigits(523);
        output += "\n";
        output += doubleDigits(-523);

    }

    // TODO:
    public int doubleDigits(int n){
        boolean negative = (n<0);
        if(negative) n = Math.abs(n);

        String nn = n+"";
        String num = ""+(Integer.parseInt(""+nn.charAt(0)))*11;
        if(nn.length()>1) num += doubleDigits(Integer.parseInt(nn.substring(1)));

        int finalNum = Integer.parseInt(num);
        if(negative) finalNum*=-1;
        return finalNum;
    }


}
