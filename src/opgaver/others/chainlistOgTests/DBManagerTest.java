package opgaver.others.chainlistOgTests;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by joneikholm on 18/09/15.
 */
public class DBManagerTest
{
    DBManager dbManagerMock1;

    LoginController loginController;
    @Before
    public void setup(){
        dbManagerMock1=mock(DBManager.class);
        loginController = new LoginController(dbManagerMock1);
    }

    @Test
    public void test(){

        loginController.checkBruger("hanna");
        verify(dbManagerMock1).checkUserStatus("hanna");
    }
    @Test
    public void testNumberOfInvocations(){

        loginController.checkBruger("hanna");
        verify(dbManagerMock1, times(1)).checkUserStatus("hanna");
    }

    @Test
    public void testArgumentValue(){
        loginController.checkBruger("Ole");
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(dbManagerMock1).checkUserStatus(captor.capture());
        assertEquals("Ole", captor.getValue());
    }

}
