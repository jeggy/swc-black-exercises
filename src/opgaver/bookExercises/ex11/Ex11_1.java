package opgaver.bookExercises.ex11;

import tools.Exercise;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by jeggy on 9/12/15.
 */
public class Ex11_1 extends Exercise{

    public Ex11_1() {
        output = sieve(25).toString();
    }















    // Returns a list of all prime numbers up to given max
    // using the sieve of Eratosthenes algorithm.
    public static List<Integer> sieve(int max) {
        List<Integer> primes = new LinkedList<Integer>();

        // add all numbers from 2 to max to a list
        List<Integer> numbers = new LinkedList<Integer>();
        for (int i = 2; i <= max; i++) {
            numbers.add(i);
        }

        while (!numbers.isEmpty()) {

            // remove a prime number from the front of the list
            int front = numbers.remove(0);
            primes.add(front);

            // remove all multiples of this prime number
            Iterator<Integer> itr = numbers.iterator();
            while (itr.hasNext()) {
                int current = itr.next();
                if (current % front == 0) {
                    itr.remove();
                }
            }
        }
        return primes;
    }
}
