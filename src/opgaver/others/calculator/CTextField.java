package opgaver.others.calculator;

import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;

/**
 * Created by jeggy on 8/27/15.
 */
public class CTextField extends TextField {

    public CTextField() {
        this.setMinSize(50, 40);
        Font t = new Font(25);
        this.setFont(t);
        this.setAlignment(Pos.CENTER_RIGHT);
        this.setEditable(false);
        this.getStyleClass().add("textField");
    }

}
